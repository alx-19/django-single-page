# Utiliser une image Python officielle
FROM python:3.9

# Installer les dépendances
COPY requirements.txt /
RUN pip install -r requirements.txt

# Ajouter le code source dans le conteneur
COPY . /app
WORKDIR /app

# Exécuter l'application
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
